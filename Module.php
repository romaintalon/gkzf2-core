<?php

namespace GKZF2\Core;

use GKZF2\Core\Error\ErrorManager;
use GKZF2\Core\ServiceConfig\ServiceConfig;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        $moduleConfig = include __DIR__ . '/config/module.config.php';
        $errorsConfig = include __DIR__ . '/config/errors.config.php';

        return array_merge($moduleConfig, $errorsConfig);
    }

    public function getAutoloaderConfig()
    {

        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getServiceConfig() {
        $config = new ServiceConfig();
        $config->addService(ServiceConfig::TYPE_FACTORIES, 'GKZF2\Core\Error\ErrorManager', function($sm) {
            $config = $sm->get('Configuration');
            return new ErrorManager($sm,
                $config['errors']['core_error_types'],
                $config['errors']['core_show_errors'],
                $config['errors']['core_error_file_destination'],
                $config['errors']['core_error_log_format']
            );
        });

        return $config->getResult();
    }
}