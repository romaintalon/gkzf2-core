<?php

return array(
    'errors' => array(
        // Generic errors (asked by Zend F2)
        'show_exceptions' => array(
            'message' => true,
            'trace' => true
        ),

        // Core errors (your specific configuration)
        'core_show_errors' => true,
        'core_error_file_destination' => '/var/log/apache2/zend-error.log',
        'core_error_log_format' => "%timestamp% %priorityName% %message%",
    )
);

