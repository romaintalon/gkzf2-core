<?php

namespace GKZF2\Core\ServiceLoader;
use GKZF2\Core\ServiceConfig\ServiceConfig;

/**
 * Interface ServiceLoaderInterface
 * @package GKZF2\Core\ServiceLoader
 */
interface ServiceLoaderInterface {
    /**
     * @param ServiceConfig $config
     * @return mixed
     */
    public function loadServices(ServiceConfig $config);
}