<?php

namespace GKZF2\Core\StringUtils;

class Generator {

    public static function string62($length = 20) {
        return self::string($length, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    }

    public static function string($length, $alphabet) {
        return substr(self::charset_base_convert(
            self::hex_string(ceil($length * sqrt(strlen($alphabet) / $length))), '0123456789abcdef', $alphabet), 0, $length);
    }

    public static function charset_base_convert($numstring, $fromcharset, $tocharset) {
        $frombase = strlen($fromcharset);
        $tobase = strlen($tocharset);
        $chars = $fromcharset;
        $tostring = $tocharset;

        $length = strlen($numstring);
        $result = '';
        $number = array();
        for ($i = 0; $i < $length; $i++) {
            $number[$i] = strpos($chars, $numstring{$i});
        }
        do {
            $divide = 0;
            $newlen = 0;
            for ($i = 0; $i < $length; $i++) {
                $divide = $divide * $frombase + $number[$i];
                if ($divide >= $tobase) {
                    $number[$newlen++] = (int) ($divide / $tobase);
                    $divide = $divide % $tobase;
                } elseif ($newlen > 0) {
                    $number[$newlen++] = 0;
                }
            }
            $length = $newlen;
            $result = $tostring{$divide} . $result;
        } while ($newlen != 0);
        return $result;
    }

    public static function hex_string($length = 32) {
        $c = bin2hex(self::random_pseudo_bytes(ceil($length / 2)));
        return substr($c, 0, $length);
    }

    public static function random_pseudo_bytes($n = 1) {
        $seed = '';
        if (!function_exists('openssl_random_pseudo_bytes')) {
            for ($i = 0; $i < $n; $i++) {
                $seed .= chr(mt_rand(0, 255));
            }
        } else {
            $seed = openssl_random_pseudo_bytes($n);
        }
        return $seed;
    }

}
