<?php

namespace GKZF2\Core\Error;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\ServiceManager;

class ErrorManager {

    protected $coreErrors;
    protected $logErrorsInFile;
    protected $logFileDestination;
    
    protected $logger;

    /**
     *
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * 
     * @param ServiceManager $serviceManager
     * @param array $coreErrors
     * @param boolean $logErrorsInFile
     * @param string $logFileDestination
     */
    function __construct(ServiceManager $serviceManager, $coreErrors = array(), 
                        $logErrorsInFile = false, $logFileDestination = '') {


        $this->serviceManager = $serviceManager;

        $this->coreErrors = $coreErrors;
        $this->logErrorsInFile = $logErrorsInFile;
        $this->logFileDestination = $logFileDestination;

        $writer = new Stream($this->logFileDestination);
        $this->logger = new Logger();
        $this->logger->addWriter($writer);
    }

    /**
     * 
     * @param int $error_code
     * @param array $extra_messages
     * @param string $type
     * @return mixed
     */
    public function generateAndLogError($error_code, $extra_messages = array(), $type = "info") {
        array_key_exists($error_code, $this->coreErrors) ?
            $error = $this->coreErrors[$error_code] :
            $error = array();

        if (!empty($extra_messages)) {
            $error['data']['extra_messages'] = $extra_messages;
        }

        if ($this->logErrorsInFile) {
            $this->generateLogInFile($error, $type);
        }

        return $error;
    }

    /**
     *
     * @param string $value
     * @param string $type
     * @return null|void
     */
    public function generateLogInFile($value, $type = "info") {
        $this->logger->$type('----------');
        $this->logger->$type(print_r($value, true));
        $this->logger->$type('----------');
    }
}
