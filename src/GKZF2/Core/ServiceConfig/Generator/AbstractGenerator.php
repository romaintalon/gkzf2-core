<?php

namespace GKZF2\Core\ServiceConfig\Generator;

use GKZF2\Core\ServiceConfig\GeneratorClass\AbstractGeneratorClass;
use GKZF2\Core\ServiceConfig\ServiceConfig;

abstract class AbstractGenerator {
    abstract public function generate(ServiceConfig $config, AbstractGeneratorClass $generatorClass);
}