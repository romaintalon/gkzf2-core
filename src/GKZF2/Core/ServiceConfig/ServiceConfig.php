<?php

namespace GKZF2\Core\ServiceConfig;

class ServiceConfig {
    const TYPE_ABSTRACT_FACTORIES = 'abstract_factories';
    const TYPE_ALIASES = 'aliases';
    const TYPE_FACTORIES = 'factories';
    const TYPE_INVOKABLES = 'invokables';
    const TYPE_SERVICES = 'services';
    const TYPE_SHARED = 'shared';
    
    protected $config = array(
        self::TYPE_ABSTRACT_FACTORIES => array(),
        self::TYPE_ALIASES => array(),
        self::TYPE_FACTORIES => array(),
        self::TYPE_INVOKABLES => array(),
        self::TYPE_SERVICES => array(),
        self::TYPE_SHARED => array(),
    );
    
    /**
     * 
     * @param string $type
     * @param string $key
     * @param mixed $value
     * @return \GKZF2\Core\ServiceConfig\ServiceConfig
     */
    
    public function addService($type, $key, $value) {
        $this->config[$type][$key] = $value;
        return $this;
    }
    
    public function getResult() {
        return $this->config;
    }
}