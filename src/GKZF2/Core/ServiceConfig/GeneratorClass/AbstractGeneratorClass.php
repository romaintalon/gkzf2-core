<?php

namespace GKZF2\Core\ServiceConfig\GeneratorClass;

abstract class AbstractGeneratorClass {
    /**
     * @return string
     */
    abstract public function getServiceName();
}