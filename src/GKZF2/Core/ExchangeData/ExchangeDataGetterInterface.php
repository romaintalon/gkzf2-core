<?php

namespace GKZF2\Core\ExchangeData;

interface ExchangeDataGetterInterface {

    /**
     * @return ExchangeData
     */
    public function getExchangeData();
}