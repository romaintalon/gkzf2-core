<?php

namespace GKZF2\Core\ExchangeData;

class ExchangeData {
    /**
     * @var array
     */
    protected $data;

    /**
     * @param $data array
     * @return $this
     */
    public function setData($data) {
        $this->data = $data;
        return $this;
    }

    /**
     * @param string $key
     * @param mixed|ExchangeData $data
     * @return $this
     */
    public function addData($key, $data) {
        $this->data[$key] = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    public static function dataToArray($data) {
        foreach ($data as $k => $d) {
            if ($d instanceOf ExchangeData) {
                $data[$k] = $d->toArray();
            } elseif(is_array($d)) {
                $data[$k] = self::dataToArray($d);
            }
        }
        return $data;
    }

    /**
     * @return array
     */
    public function toArray() {
        $data = $this->data;
        if ($data !== null) {
            if (is_array($data)) {
                return self::dataToArray($data);
            } else {
                return $data;
            }
        } else {
            return null;
        }
    }

    /**
     * @param ExchangeDataGetterInterface[] $exchangeDataGetterInterfaces
     * @return ExchangeData
     */
    public static function exchangeDataGetterInterfaceListToExchangeData($exchangeDataGetterInterfaces) {
        $data = array();
        foreach ($exchangeDataGetterInterfaces as $exchangeDataGetterInterface) {
            $data[] = $exchangeDataGetterInterface->getExchangeData();
        }
        $exchangeData = new ExchangeData();
        $exchangeData->setData($data);
        return $exchangeData;
    }
}