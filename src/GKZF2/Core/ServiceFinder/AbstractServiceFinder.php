<?php

namespace GKZF2\Core\ServiceFinder;

use Zend\ServiceManager\ServiceManager;

class AbstractServiceFinder {

    /** @var  ServiceManager */
    protected $serviceManager;

    /**
     * @param ServiceManager $sm
     */
    public function __construct(ServiceManager $sm) {
        $this->setServiceManager($sm);
    }

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }
}