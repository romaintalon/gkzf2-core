<?php

namespace GKZF2\Core\Processor;

use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

abstract class AbstractProcessor {
    /** @var MvcEvent $mvcEvent */
    private $mvcEvent;
    
    /** @var array $configuration */
    private $configuration;
    
    /** @var array $eventParams */
    private $eventParams;
    
    protected function getMvcEvent() {
        return $this->mvcEvent;
    }

    protected function getConfiguration() {
        return $this->configuration;
    }

    protected  function getEventParams() {
        return $this->eventParams;
    }
    
    public function setProperties(MvcEvent $mvcEvent) {
        $this->mvcEvent = $mvcEvent;
        $this->configuration = $mvcEvent->getApplication()->getServiceManager()->get('Config');
//        $this->configuration = $mvcEvent->getApplication()->getConfig();
        $this->eventParams = $mvcEvent->getParams();
    }

    /**
     * @return ServiceManager|ServiceLocatorInterface
     */
    public function getServiceManager() {
        if (null !== $this->mvcEvent) {
            if (!method_exists($this->mvcEvent->getTarget(), 'getServiceLocator') ||
                    null === $this->mvcEvent->getTarget()->getServiceLocator()) {
                return $this->mvcEvent->getApplication()->getServiceManager();
            }
            else {
                return $this->mvcEvent->getTarget()->getServiceLocator();
            }
        }
        
        return null;
    }
    
    public abstract function processAndGetResponse(MvcEvent $e);
}
