<?php

namespace GKZF2\Core\Processor;

use \Zend\Http\Response;

abstract class AbstractPostProcessor {
    
    protected $_response;
    protected $_statusCode;
    protected $_data;

    /**
     * @param Response $response
     * @param $statusCode
     * @param array $data
     */
    public function __construct(Response $response, $statusCode, $data = null) {
        $this->_response = $response;
        $this->_statusCode = $statusCode;
        $this->_data = $data;
    }

    abstract function processAndGetResponse();
}
